package hello;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import org.bson.Document;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

class ExportToCsv {
	
	public void appendData(String path) {
		
		File file = new File(path);
		try {
            
			FileWriter outputfile = new FileWriter(file);

			BufferedWriter writer = new BufferedWriter(outputfile);

			String uri =System.getenv("MONGO_URI");
			
			
			Properties prop = new Properties();
			if(uri==null) {

				try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
					

					if(input==null) 
						System.out.println("Sorry!config.properties file not found");

					else {

						prop.load(input);

						uri = "mongodb://"+prop.getProperty("db.username")+":"+prop.getProperty("db.password")+"@"+prop.getProperty("db.host")+prop.getProperty("db.port");
						
					}
					
				} catch (IOException ex) {
					ex.printStackTrace();
				}

			}
			
			MongoClient mongoClient = MongoClients.create(uri); 
			
            prop.load(new FileInputStream("src/main/resources/config.properties"));
            MongoDatabase database = mongoClient.getDatabase(prop.getProperty("db.dbname"));

			MongoCollection<Document> collection = database.getCollection("bookstore");

			//write in csv file
			try (MongoCursor<Document> cur = collection.find().iterator()) {
				int iterate=1;

				while (cur.hasNext()) {

					Document doc = cur.next();
					if(iterate==1) {

						ArrayList key = new ArrayList<>(doc.keySet());
						for(Object k: key) {
							writer.write(k+",");
						}
						writer.write("\n");

					}

					ArrayList booklist = new ArrayList<>(doc.values());
					for(Object value: booklist) {
						writer.write(value+",");
						
					}
					writer.write("\n");

					iterate++;

				}
			}

			writer.close();
			
		}
		catch (Exception e) {
			System.out.println(e);
			
		}
	}
	
	
	public void getCsvFilePath() {

		// get file path of csv
		String path = System.getenv("CSV_FILEPATH");
		if(path!=null) {
			appendData(path);
		}
		else {
			try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {

				Properties prop = new Properties();

				if(input==null) 
					System.out.println("Sorry!config.properties file not found");

				else {

					prop.load(input);

					path = prop.getProperty("csvfilepath");

					appendData(path);
				}  
			}
			catch (IOException ex) {
				ex.printStackTrace();
			}

		}
	}

	public static void main(String[] args) {
		ExportToCsv obj = new ExportToCsv();
		obj.getCsvFilePath();
	}

}