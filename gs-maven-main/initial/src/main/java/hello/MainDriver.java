package hello;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

public class MainDriver {

	public void insertValues(String finalUri) {

		MongoClient mongoClient = MongoClients.create(finalUri);	

		try(InputStream input = new FileInputStream("src/main/resources/config.properties")) {

			Properties prop = new Properties();

			if(input==null) 
				System.out.println("Sorry!config.properties file not found");

			else {

				prop.load(input);

				MongoDatabase db = mongoClient.getDatabase(prop.getProperty("db.dbname"));

				MongoCollection<Document> collection_name = db.getCollection("bookstore");

				for(int i=0;i<10000;i++) {

					Document doc = new Document("Book name","Harry Potter").append("Quantity",12).append("Author","JK Rowling");

					collection_name.insertOne(doc);
				}
				
				// to export mongodb to csv
				new ExportToCsv().getCsvFilePath();

			}

		}
		catch(IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	public void createClient(){

		String uri =System.getenv("MONGO_URI");
		if(uri!=null) {

			insertValues(uri);

		}
		else {
			try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {

				Properties prop = new Properties();

				if(input==null) 
					System.out.println("Sorry!config.properties file not found");

				else {

					prop.load(input);

					uri = "mongodb://"+prop.getProperty("db.username")+":"+prop.getProperty("db.password")+"@"+prop.getProperty("db.host")+prop.getProperty("db.port");

					insertValues(uri);
				}

			} catch (IOException ex) {
				ex.printStackTrace();
			}


		}
	}


	public void getChoice() {

		System.out.println("Enter 1 to insert values\nType 2 to exit");
		Scanner sc = new Scanner(System.in);
		while(true) {
			int input = sc.nextInt();
			if(input==1) {

				MainDriver obj = new MainDriver();
				obj.createClient();
			}
			else if(input==2) {
				break;
			}
			else {
				System.out.println("Enter valid number");
			}
		}
	}
	public static void main(String[] args) {

		long start = System.currentTimeMillis();

		MainDriver getInp = new MainDriver();
		getInp.getChoice();

		long end = System.currentTimeMillis() - start;
		System.out.println("Time taken to complete the process (in Milliseconds):"+end);
		float time= end/(float)1000;
		System.out.println("Time taken to complete the process (in Seconds):"+time);
	}
}
